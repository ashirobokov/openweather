package ru.ashirobokov.android.openweather.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 1 on 02.12.2016.
 */
public class Sys {

    @SerializedName("population")
    @Expose
    private Integer population;

    /**
     *
     * @return
     * The population
     */
    public Integer getPopulation() {
        return population;
    }

    /**
     *
     * @param population
     * The population
     */
    public void setPopulation(Integer population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "Sys{" +
                "population=" + population +
                '}';
    }

}
