package ru.ashirobokov.android.openweather.data;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.ashirobokov.android.openweather.R;
import ru.ashirobokov.android.openweather.model.WeatherForecastItem;
import ru.ashirobokov.android.openweather.ui.MainActivity;
import ru.ashirobokov.android.openweather.utils.ConstantManager;

/**
 * Created by ashirobokov on 02.12.2016.
 */
public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {

    private static final String TAG = ConstantManager.TAG_PREFIX + "Weather Adapter";

    public static class WeatherViewHolder extends RecyclerView.ViewHolder {

        CardView weatherView;
        ImageView mPicture;
        TextView mDate;
        TextView mTemperature;
        TextView mDescription;

        WeatherViewHolder(View itemView) {
            super(itemView);

            weatherView = (CardView) itemView.findViewById(R.id.card_view);
            mPicture = (ImageView) itemView.findViewById(R.id.weather_image);
            mDate = (TextView) itemView.findViewById(R.id.date_time);
            mTemperature = (TextView) itemView.findViewById(R.id.temperature);
            mDescription = (TextView) itemView.findViewById(R.id.weather_description);

        }


    }

    RecyclerView mRecyclerView;
    List<WeatherForecastItem> weathers;
    Context context;

    public WeatherAdapter(List<WeatherForecastItem> weathers, RecyclerView recyclerView, Context context) {
            this.weathers = weathers;
            this.mRecyclerView = recyclerView;
            this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_weather, viewGroup, false);

        WeatherViewHolder weatherViewHolder = new WeatherViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int pos = mRecyclerView.getChildAdapterPosition(v);
                if (pos >= 0 && pos < getItemCount()) {
                    WeatherForecastItem weatherItem = weathers.get(pos);
                    Log.d(TAG, weatherItem.toString());
                    WeatherItemData weatherData = getWeatherItem(weatherItem);

                    if (context instanceof MainActivity) {
                        MainActivity activity = (MainActivity) context;
                        activity.goWeatherItem(weatherData);
                    }

                }
            }
        });

    return weatherViewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder weatherViewHolder, int i) {

        String imageName = weathers.get(i).getWeather().get(0).getIcon();
        int imageId = context.getResources().getIdentifier("ic_"+imageName, "mipmap", context.getPackageName());
        weatherViewHolder.mPicture.setImageResource(imageId);

        int temperature = (int) Math.round(weathers.get(i).getMain().getTemp());

        weatherViewHolder.mDate.setText(weathers.get(i).getDtTxt());
        weatherViewHolder.mTemperature.setText(String.format("%d", temperature));
        weatherViewHolder.mDescription.setText(weathers.get(i).getWeather().get(0).getDescription());


        if (temperature < 0)
            weatherViewHolder.weatherView.setCardBackgroundColor(context.getResources().getColor(R.color.colorBlue));
        else
            weatherViewHolder.weatherView.setCardBackgroundColor(context.getResources().getColor(R.color.colorOrangeLight));

    }


    @Override
    public int getItemCount() {
        return weathers.size();
    }

    private WeatherItemData getWeatherItem (WeatherForecastItem weather) {

            String dateTime = weather.getDtTxt();
            Double temperature = weather.getMain().getTemp();
            String weatherImage = weather.getWeather().get(0).getIcon();
            String weatherDescription = weather.getWeather().get(0).getDescription();
            Double windSpeed = weather.getWind().getSpeed();
            Double windDeg = weather.getWind().getDeg();
            Integer humidity = weather.getMain().getHumidity();
            Double pressure = weather.getMain().getPressure();
            Integer cloudiness = weather.getClouds().getAll();

            WeatherItemData data = new WeatherItemData(dateTime, temperature, weatherImage, weatherDescription,
                    windSpeed, windDeg, humidity, pressure, cloudiness);


    return data;
    }


}
