package ru.ashirobokov.android.openweather.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ashirobokov.android.openweather.R;
import ru.ashirobokov.android.openweather.data.WeatherAdapter;
import ru.ashirobokov.android.openweather.data.WeatherItemData;
import ru.ashirobokov.android.openweather.model.Forecast;
import ru.ashirobokov.android.openweather.model.WeatherForecastItem;
import ru.ashirobokov.android.openweather.service.OpenWeatherApi;
import ru.ashirobokov.android.openweather.utils.ConstantManager;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends ProgessActivity {

    static final String TAG = ConstantManager.TAG_PREFIX + "MainActiv.";

    private Toolbar mToolBar;
    private RecyclerView recyclerView;
    private List<WeatherForecastItem> mList;
    private String mJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_layout);
        mToolBar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(mToolBar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        initData();

    }


    private void initData() {

/*
        //  Отладочный вариант (stub) :: Из mock файла
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Resources res = this.getResources();

        GetJsonString jsonObj = new GetJsonString(res);
        mJson = jsonObj.getJson();

        Forecast weatherForecast = gson.fromJson(mJson, Forecast.class);
        mList = weatherForecast.getList();
*/


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeatherApi service = retrofit.create(OpenWeatherApi.class);

        showProgress();

        Observable<Response<Forecast>> weather = service.getCurrentWeather();

        weather.subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(new Subscriber<Response<Forecast>>() {
                   @Override
                   public void onCompleted() {
                       Log.d(TAG, "Completed :-)");
                   }

                   @Override
                   public void onError(Throwable t) {
                       hideProgress();
                       Log.d(TAG, t.toString());
                   }

                   @Override
                   public void onNext(Response<Forecast> response) {
                       if (response.code() == 200) {

                           try {

                               mList = response.body().getList();
                               Log.d(TAG + " RxJava ", mList.toString());
                               initAdapter(mList, recyclerView, MainActivity.this);

                           } catch (NullPointerException e) {
                               Log.d(TAG, e.toString());
                           }

                       } else if (response.code() == 404) {
                           Log.d(TAG, "Неверный логин или пароль");
                       } else {
                           Log.d(TAG, "Другая ошибка");
                           if (ConstantManager.DEBUG) {
                               Log.d(TAG, String.valueOf(response.code()));
                           }

                       }
                       hideProgress();
                   }

               });

    }


    private void initAdapter(List<WeatherForecastItem> list, RecyclerView recyclerView, Context context) {

        WeatherAdapter adapter = new WeatherAdapter(list, recyclerView, context);
        recyclerView.setAdapter(adapter);

    }


    public void goWeatherItem(WeatherItemData data) {

        Intent weatherIntent = new Intent(MainActivity.this, WeatherItemActivity.class);
        weatherIntent.putExtra(ConstantManager.PARCELABLE_KEY, data);
        startActivity(weatherIntent);

    }

}
