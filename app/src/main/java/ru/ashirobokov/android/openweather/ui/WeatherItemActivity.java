package ru.ashirobokov.android.openweather.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import ru.ashirobokov.android.openweather.R;
import ru.ashirobokov.android.openweather.data.WeatherItemData;
import ru.ashirobokov.android.openweather.utils.ConstantManager;

/**
 * Created by 1 on 16.12.2016.
 */
public class WeatherItemActivity extends AppCompatActivity {

    private Toolbar mToolBar;

    final String TAG = ConstantManager.TAG_PREFIX + "WeatherItem Activity";
    private TextView mCurrentDateTime;
    private TextView mCurrentTemp;
    private TextView mWeatherType;
    private ImageView mWeatherImage;
    private TextView mHumidity;
    private TextView mPressure;
    private TextView mWindValues;
    private TextView mClouds;
    private ImageView mTemperatureImage;
    private ImageView mCelsiusImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.coordinator_weather);

        mToolBar = (Toolbar) findViewById(R.id.app_weather_toolbar);
        setSupportActionBar(mToolBar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mCurrentDateTime = (TextView) findViewById(R.id.current_date_time);
        mCurrentTemp = (TextView) findViewById(R.id.current_temp);
        mWeatherType = (TextView) findViewById(R.id.weather_type);
        mWeatherImage = (ImageView) findViewById(R.id.weather_image);
        mHumidity = (TextView) findViewById(R.id.humidity_value);
        mPressure = (TextView) findViewById(R.id.pressure_value);
        mWindValues = (TextView) findViewById(R.id.wind_values);
        mClouds = (TextView) findViewById(R.id.clouds_value);

        mTemperatureImage = (ImageView) findViewById(R.id.temperature_pic);
        mCelsiusImage = (ImageView)findViewById(R.id.celcius_pic);

        final WeatherItemData weather = getIntent().getParcelableExtra(ConstantManager.PARCELABLE_KEY);
        displayWeather(weather);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void displayWeather(WeatherItemData weather) {


     mCurrentDateTime.setText(getDateString(weather.getDateTime()));

/*
*   Блок Погода, включает в себя :
*   -   выбор картинки
*   -   описание погоды словами
*   -
*/

        String weatherType = weather.getWeatherDescription();
        mWeatherType.setText(weatherType);

        String imageName = weather.getWeatherImageName();
        if (!imageName.isEmpty()) {
            int imageId = getResources().getIdentifier("ic_"+imageName, "mipmap", getPackageName());
            mWeatherImage.setImageDrawable(getDrawable(imageId));
        }
        else {
            mWeatherImage.setImageDrawable(getDrawable(R.mipmap.ic_121_undef));
        }


/*
*   Блок Основной (Main), включает в себя :
*   -   температуру
*   -   давление
*   -   влажность
*/

        int tempInt = (int) Math.round(weather.getTemperature());
        String temperature = Integer.toString(tempInt);
        mCurrentTemp.setText(temperature);

        if (tempInt < 0) {
            mTemperatureImage.setImageResource(R.drawable.ic_temp_moroz_size_64);
            mCelsiusImage.setImageResource(R.drawable.ic_celsius_moroz_size64);
        }

        int pressureInt = (int) Math.round(weather.getPressure());
        String pressure = Integer.toString(3*pressureInt/4) + "мм. рт. столба";
        mPressure.setText(pressure);

        String humidity = Integer.toString(weather.getHumidity()) + "%";
        mHumidity.setText(humidity);


/*
*   Блок Ветер, включает в себя :
*   -   скорость м/с
*   -   направление
*/
        Integer windSpeed = (int) Math.round(weather.getWindSpeed());
        Integer windDegree = (int) Math.round(weather.getWindDeg());
        String wind = Integer.toString(windSpeed) + "м/с  " + getRumb(windDegree);
        mWindValues.setText(wind);


/*
*   Блок Clouds, включает в себя :
*   -   облачность в процентаx
*
*/

      String clouds = Integer.toString(weather.getCloudiness()) + "%";
      mClouds.setText(clouds);

    }

    /*
    *   Пересчет 360 градусов горизонта в румбы горизонта
    *    > 355 < 5      Север
    *    >= 5  <= 85    Северо-Восточный
    *    > 85  < 95     Восточный
    *    >= 95 <=175    Юго-Восточный
    *    > 175 < 185    Южный
    *    >= 185 <= 265	Юго-Западный
    *    > 265  < 275   Западный
    *    >= 275 <= 355  Северо-Западный
    */
    private String getRumb(int degree) {
        String rumb = "";

//        System.out.println("degree = " + degree);

        if (degree > 355 || degree < 5 )
            rumb ="(С)";
        if (degree >= 5 && degree <= 85 )
            rumb ="(С-В)";
        if (degree > 85 && degree < 95 )
            rumb ="(В)";
        if (degree >= 95 && degree <= 175 )
            rumb ="(Ю-В)";
        if (degree > 175 && degree < 185)
            rumb ="(Ю)";
        if (degree >= 185 && degree <= 265)
            rumb ="(Ю-З)";
        if (degree > 265 && degree < 275)
            rumb ="(З)";
        if (degree >= 275 && degree <= 355)
            rumb ="(С-З)";

        return rumb;
    }

    private String getDateString(String dateTime){
//  "dt_txt":"2016-11-29 18:00:00"

    String itemDay = "", itemMonth = "", itemYear = "", itemTime = "";

        int mpos = dateTime.indexOf("-") + 1;
        int dpos = dateTime.indexOf("-", mpos) + 1;
        int tpos = dateTime.indexOf(" ") + 1;

        itemDay = dateTime.substring(dpos, dpos + 2);
        itemMonth = dateTime.substring(mpos, mpos + 2);
        itemYear = dateTime.substring(0, 4);
        itemTime = dateTime.substring(tpos);
        String monthStri = getCalendarMonth(Integer.parseInt(itemMonth));

        String dateStri = itemDay + " " + monthStri + " " + itemYear + " " + itemTime;
        System.out.println("DateTime: " + dateStri);

        return dateStri;
    }

    private String getDateString(Calendar calendar){
        String dateStri =  Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + " " +
                getCalendarMonth(calendar) + " " +
                String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
                String.format("%02d", calendar.get(Calendar.MINUTE));

        return dateStri;
    }

    private String getCalendarMonth(int month) {
        String monthStri = "";

        switch (month) {
            case 1: monthStri = "Января";
                break;
            case 2: monthStri = "Февраля";
                break;
            case 3: monthStri = "Марта";
                break;
            case 4: monthStri = "Апреля";
                break;
            case 5: monthStri = "Мая";
                break;
            case 6: monthStri = "Июня";
                break;
            case 7: monthStri = "Июля";
                break;
            case 8: monthStri = "Августа";
                break;
            case 9: monthStri = "Сентября";
                break;
            case 10: monthStri = "Октября";
                break;
            case 11: monthStri = "Ноября";
                break;
            case 12: monthStri = "Декабря";
                break;

        }

        return monthStri;
    }

    private String getCalendarMonth(Calendar calendar) {
        String monthStri = "";

        int month = calendar.get(Calendar.MONTH) + 1;

        switch (month) {
            case 1: monthStri = "Января";
                break;
            case 2: monthStri = "Февраля";
                break;
            case 3: monthStri = "Марта";
                break;
            case 4: monthStri = "Апреля";
                break;
            case 5: monthStri = "Мая";
                break;
            case 6: monthStri = "Июня";
                break;
            case 7: monthStri = "Июля";
                break;
            case 8: monthStri = "Августа";
                break;
            case 9: monthStri = "Сентября";
                break;
            case 10: monthStri = "Октября";
                break;
            case 11: monthStri = "Ноября";
                break;
            case 12: monthStri = "Декабря";
                break;

        }

        return monthStri;
    }

}
