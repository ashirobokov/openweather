package ru.ashirobokov.android.openweather.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AShirobokov on 15.12.2016.
 */
public class WeatherItemData implements Parcelable {
    private String dateTime;
    private Double temperature;
    private String weatherImageName;
    private String weatherDescription;
    private Double windSpeed;
    private Double windDeg;
    private Integer humidity;
    private Double pressure;
    private Integer cloudiness;

    public WeatherItemData() {

    }

    public WeatherItemData(String dateTime, Double temperature, String weatherImageName, String weatherDescription,
                           Double windSpeed, Double windDeg, Integer humidity, Double pressure, Integer cloudiness) {

            this.dateTime = dateTime;
            this.temperature = temperature;
            this.weatherImageName = weatherImageName;
            this.weatherDescription = weatherDescription;
            this.windSpeed = windSpeed;
            this.windDeg = windDeg;
            this.humidity = humidity;
            this.pressure = pressure;
            this.cloudiness = cloudiness;
    }

    protected WeatherItemData(Parcel in) {

            this.dateTime = in.readString();
            this.temperature = in.readDouble();
            this.weatherImageName = in.readString();
            this.weatherDescription = in.readString();
            this.windSpeed = in.readDouble();
            this.windDeg = in.readDouble();
            this.humidity = in.readInt();
            this.pressure = in.readDouble();
            this.cloudiness = in.readInt();

    }

    public static final Creator<WeatherItemData> CREATOR = new Creator<WeatherItemData>() {
        @Override
        public WeatherItemData createFromParcel(Parcel in) {
            return new WeatherItemData(in);
        }

        @Override
        public WeatherItemData[] newArray(int size) {
            return new WeatherItemData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

            dest.writeString(dateTime);
            dest.writeDouble(temperature);
            dest.writeString(weatherImageName);
            dest.writeString(weatherDescription);
            dest.writeDouble(windSpeed);
            dest.writeDouble(windDeg);
            dest.writeInt(humidity);
            dest.writeDouble(pressure);
            dest.writeInt(cloudiness);
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public String getWeatherImageName() {
        return weatherImageName;
    }

    public void setWeatherImageName(String weatherImageName) {
        this.weatherImageName = weatherImageName;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Double getWindDeg() {
        return windDeg;
    }

    public void setWindDeg(Double windDeg) {
        this.windDeg = windDeg;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Integer getCloudiness() {
        return cloudiness;
    }

    public void setCloudiness(Integer cloudiness) {
        this.cloudiness = cloudiness;
    }

    @Override
    public String toString() {
        return "WeatherItemData{" +
                "dateTime='" + dateTime + '\'' +
                ", temperature=" + temperature +
                ", weatherImageName='" + weatherImageName + '\'' +
                ", weatherDescription='" + weatherDescription + '\'' +
                ", windSpeed=" + windSpeed +
                ", windDeg=" + windDeg +
                ", humidity=" + humidity +
                ", pressure=" + pressure +
                ", cloudiness=" + cloudiness +
                '}';
    }
}
