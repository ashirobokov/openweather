package ru.ashirobokov.android.openweather.ui;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

import ru.ashirobokov.android.openweather.R;
import ru.ashirobokov.android.openweather.utils.ConstantManager;

/**
 * Created by AShirobokov on 15.12.2016.
 */
public class ProgessActivity extends AppCompatActivity {

    private static final String TAG = ConstantManager.TAG_PREFIX + "ProgressActivity";
    protected ProgressDialog mProgressDialog;

    public void showProgress() {


        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        }

        mProgressDialog.show();

    }

    public void hideProgress() {
        if (mProgressDialog != null)
            if (mProgressDialog.isShowing())
                mProgressDialog.hide();
    }

}
